import * as moment from "moment";
import { db } from "../db";
import { testCurrencies, testExchangeRates } from "./exchange";

export async function generateTestData() {
    db.currencies.bulkAdd(testCurrencies);
    db.exchangeRates.bulkAdd(testExchangeRates);

    const tslaId = await db.positions.add(
    {
            name: 'TSLA',
            currentQuantity: 10000,
            closed: 0,
            tags: ['us', 'tecH']
    });
    const legId = await db.positions.add(
    {
            name: 'LEG',
            currentQuantity: 5,
            closed: 0,
            tags: ['EU', '???']
    });
    db.marketValueHistory.bulkAdd([
        {
            positionId: legId,
            date: moment.now() - 60 * 1000 * 60 * 60 * 24,
            value: 20.2222222222,
            currencyId: 1
        },
        {
            positionId: legId,
            date: moment.now() - 30 * 1000 * 60 * 60 * 24,
            value: 24.98632456,
            currencyId: 1
        },
        {
            positionId: tslaId,
            date: moment.now() - 30 * 1000 * 60 * 60 * 24,
            value: 1000,
            currencyId: 1
        }
    ]);
    db.transactionHistory.bulkAdd([
        {
            toId: tslaId,
            fromQty: 10000*1000,
            toQty: 10000,
            date: moment.now(),
            fee: 0,
            feeCurrencyId: 1
        },
        {
            toId: legId,
            fromQty: 20.2222222222*10,
            toQty: 10,
            date: moment.now() - 30 * 1000 * 60 * 60 * 24,
            fee: 10,
            feeCurrencyId: 1
        },
        {
            fromId: legId,
            fromQty: 5,
            toQty: 24.98632456 * 5,
            date: moment.now() - 5 * 1000 * 60 * 60 * 24,
            fee: 0,
            feeCurrencyId: 1
        }
    ]);
    db.portfolioBuckets.bulkAdd([
        {
            name: 'Diversification per continent',
            allocations: [{tag: 'EU', percentage: 30}, {tag: 'US', percentage: 70}]
        },
        {
            name: 'Diversification per type',
            allocations: [{tag: 'Tech', percentage: 10}, {tag: 'Commodity', percentage: 60}, {tag: 'Debt', percentage: 30}]
        }
    ]);
  }