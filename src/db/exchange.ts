import * as moment from "moment";

export interface ExchangeRate {
    fromCurrencyId: number,
    toCurrencyId: number,
    rate: number,
    date: number
}

export interface Currency {
    id?: number,
    name: string
}

export const defaultCurrencies: Currency[] = [{
    id: 1,
    name: "EUR"
}, {
    id: 2,
    name: "USD"
}, {
    id: 3,
    name: "CZK"
}];

export const testCurrencies: Currency[] = [];

export const testExchangeRates: ExchangeRate[] = [{
    fromCurrencyId: 1,
    toCurrencyId: 2,
    rate: 1.1,
    date: moment.now()
}, {
    fromCurrencyId: 3,
    toCurrencyId: 1,
    rate: 0.05,
    date: moment.now()
}];