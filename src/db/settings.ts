export enum ExchangeRateApiType {
    NO_API,
    ABSTRACT_API = "Abstract API"
}

export const exchangeRateApiList: ExchangeRateApiType[] = [ExchangeRateApiType.ABSTRACT_API];

export interface ExchangeRateApi {
    api: ExchangeRateApiType,
    key?: string
}

export interface Settings {
    id: number,
    mainCurrencyId: number,
    staleDayCount: number,
    exchangeRateApi?: ExchangeRateApi
};

export const defaultSettings: Settings = {
    id: 1,
    exchangeRateApi: {api: ExchangeRateApiType.NO_API},
    mainCurrencyId: null,
    staleDayCount: null
}