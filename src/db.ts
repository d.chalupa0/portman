// db.ts
import Dexie, { Table } from 'dexie';
import { defaultCurrencies, ExchangeRate, Currency } from './db/exchange';
import { defaultSettings, Settings } from './db/settings'
import "dexie-export-import";

export interface Position {
    id?: number;
    name: string;
    currentQuantity: number;
    closed: number; // Dexie cannot use boolean, so 1=true, 0=false
    tags?: string[]
}

export interface MarketValue {
    id?: number;
    positionId: number;
    date: number;
    value: number;
    currencyId: number;
}

export interface Transaction {
    id?: number;
    fromId?: number;
    toId?: number;
    fromQty: number;
    toQty: number;
    date: number;
    fee: number;
    feeCurrencyId: number;
    comment?: string;
}

export interface BucketAllocation {
    tag: string,
    percentage: number
}

export interface PortfolioBucket {
    id?: number;
    name: string;
    allocations?: BucketAllocation[];
}

export class AppDB extends Dexie {
  positions!: Table<Position, number>;
  transactionHistory!: Table<Transaction, number>;
  marketValueHistory!: Table<MarketValue, number>;
  portfolioBuckets!: Table<PortfolioBucket, number>;
  currencies!: Table<Currency, number>;
  exchangeRates!: Table<ExchangeRate, number>;
  settings!: Table<Settings, number>;

  constructor(private fromImport: boolean) {
    super('ngDexiePortmanData');
    this.version(1).stores({
      positions: '++id, closed',
      transactionHistory: '++id, fromId, toId, date',
      marketValueHistory: '++id, positionId, date',
      portfolioBuckets: '++id, name',
      currencies: '++id, name',
      exchangeRates: '++id, fromCurrencyId, toCurrencyId, date',
      settings: 'id'
    });
    this.on('populate', () => this.populate());
  }

  async populate() {
    if (this.fromImport) {
        return;
    }
    db.settings.add(defaultSettings);
  }
}

export async function importDb(file) {
    await db.delete();
    db = new AppDB(true);
    await db.import(file, {});

    alert('Import finished');
}

export let db = new AppDB(false);
