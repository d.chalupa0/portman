import { Component, OnInit } from '@angular/core';
import { PortfolioBucket } from '../../db';
import { bucketService } from '../services/bucket.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-buckets',
  templateUrl: './buckets.component.html',
  styleUrls: ['./buckets.component.scss']
})
export class BucketsComponent implements OnInit {

  buckets: PortfolioBucket[];

  newBucketVisible: boolean = false;
  bucketForm: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit(): void {
    bucketService.getBuckets().then((bs) => (this.buckets = bs))

    this.bucketForm = this.fb.group({
      name: [null, [Validators.required]]
    });
  }

  onAddNewBucketClicked() {
    this.newBucketVisible = true;
  }

  onFormExit() {
    this.bucketForm.reset();
    this.newBucketVisible = false;
  }

  onFormKeyUp(e) {
    if (e.keyCode === 27) {
      this.onFormExit();
    }
  }

  isNameValid(noErrWhenUntouched: boolean = true) {
    return (noErrWhenUntouched && !this.bucketForm.get("name").touched)
           || this.bucketForm.get("name").errors === null;
  }

  onAddNewBucketSubmit() {
    this.bucketForm.markAllAsTouched();

    if (!this.isNameValid(false))
    {
      return;
    }

    const newBucket = this.bucketFormToBucket();
    bucketService.addBucket(newBucket)
    .then((b) => this.buckets.push({id: b, ...newBucket}));
    this.newBucketVisible = false;
  }

  bucketFormToBucket(): PortfolioBucket {
    return {
      name: this.bucketForm.value.name,
      allocations: []
    }
  }

  deleteBucket(b: PortfolioBucket) {
    bucketService.deleteBucket(b);
    this.buckets = this.buckets.filter((bucket) => bucket.id !== b.id);
  }
}
