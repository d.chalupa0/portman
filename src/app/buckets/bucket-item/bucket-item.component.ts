import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BucketAllocation, PortfolioBucket } from '../../../db';
import { positionService, PositionWithValue } from '../../services/position.service';

import {
  ApexNonAxisChartSeries,
  ApexResponsive,
  ApexChart,
  ChartComponent,
  ApexTitleSubtitle,
  ApexDataLabels,
  ApexLegend
} from "ng-apexcharts";

import { bucketService } from '../../services/bucket.service';
import { combineLatest, mergeMap, of, Subscription } from 'rxjs';
import { settingsService } from '../../services/settings.service';
import { Currency, ExchangeRate } from '../../../db/exchange';
import { ExchangeService } from '../../services/exchange.service';
import { getDifferenceInDays } from '../../utils/date-utils';
import * as moment from 'moment';

interface AllocationItem extends BucketAllocation {
  totalValuation: number,
  anythingOutdated: boolean,
  relevantPositions?: {name: string, value: {value: number, outdated: boolean}}[]
}

export type ChartOptions = {
  series: ApexNonAxisChartSeries;
  chart: ApexChart;
  responsive: ApexResponsive[];
  labels: string[]
  title: ApexTitleSubtitle;
  dataLabels: ApexDataLabels;
  legend: ApexLegend;
};

@Component({
  selector: '[app-bucket-item]',
  templateUrl: './bucket-item.component.html',
  styleUrls: ['./bucket-item.component.scss']
})

export class BucketItemComponent implements OnInit {
  @Input() bucket: PortfolioBucket
  @Output() onDelete: EventEmitter<PortfolioBucket> = new EventEmitter()

  @ViewChild('myInput') myInput: ElementRef;
  @ViewChild("expected") expectedPie: ChartComponent;
  @ViewChild("current") currentDonut: ChartComponent;
  public expectedPieOptions: Partial<ChartOptions>;
  public currentDonutOptions: Partial<ChartOptions>;
  showExpectedChart: boolean = false;
  showCurrentChart: boolean = false;

  allocations: AllocationItem[] = []
  bucketValuation: number;

  allocationFormVisible: boolean = false;
  allocationForm: FormGroup

  missingPercentage: number = 0;

  mainCurrency: Currency;
  positions: PositionWithValue[];

  dataSubscription: Subscription;

  staleDays: number;

  constructor(private fb: FormBuilder, private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    const tags = this.bucket.allocations.map(a => a.tag);
  
    this.expectedPieOptions = {
      series: this.bucket.allocations.map(a => a.percentage),
      chart: {
        id: 'expected',
        group: 'pies',
        animations: {
          enabled: false
        },
        width: 380,
        type: "pie"
      },
      title:{
        text: "Expected",
        align: "center",
        style: {
          color:  'white'
        },
      },
      labels: tags,
      dataLabels: {
        enabled: true,
        formatter: function (val: number, { seriesIndex, dataPointIndex, w }) {
          return [`${w.globals.seriesNames[seriesIndex]}`,`\r\n${val.toFixed(2)} %`] as (unknown) as (string | number);
        }
      },
      legend: {show: false, onItemHover: {highlightDataSeries: false}},
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };
    this.currentDonutOptions = {
      series: tags.map(() => 0),
      chart: {
        id: 'current',
        group: 'pies',
        width: 380,
        type: "pie",
        animations: {
          enabled: false
        }
      },
      title:{
        text: "Current",
        align: "center",
        style: {
          color:  'white'
        },
      },
      labels: tags,
      dataLabels: {
        enabled: true,
        formatter: function (val: number, { seriesIndex, dataPointIndex, w }) {
          return [`${w.globals.seriesNames[seriesIndex]}`,`\r\n${val.toFixed(2)} %`] as (unknown) as (string | number);
        }
      },
      legend: {show: false, onItemHover: {highlightDataSeries: false}},
      responsive: [
        {
          breakpoint: 480,
          options: {
            chart: {
              width: 200
            },
            legend: {
              position: "bottom"
            }
          }
        }
      ]
    };

    this.tagsChanged();

    this.allocationForm = this.fb.group({
      tag: [null, [Validators.required]],
      percentage: [null, [Validators.required]]
    });
  }

  tagsChanged() {

    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }

    const mainSub = combineLatest([ settingsService.observeMainCurrency(),
      positionService.getPositionsByTags( this.bucket.allocations.map(a => a.tag) ),
      settingsService.observeStaleDayCount() ])

    this.dataSubscription = mainSub.pipe(
        mergeMap(([currency, positions, staleDays]) => (
            this.mainCurrency = currency,
            this.positions = positions,
            this.staleDays = staleDays,
            this.positions.length > 0 ?
              combineLatest( this.positions.map( (p) =>
                p.lastMarketValue ?
                  this.exchangeService.observeClosestRate(p.lastMarketValue.currencyId, this.mainCurrency.id, p.lastMarketValue.date)
                  : of(<ExchangeRate>undefined) ) )
              : of(<ExchangeRate[]>[])
        ))
      ).subscribe((rates) => (
        this.mapAllocationsUsingRates(rates),
        this.updateChartData(),
        this.updateSumPerc())
      );
  }

  mapAllocationsUsingRates( rates: ExchangeRate[] ) {
    this.allocations = []
    for (let alo of this.bucket.allocations) {
      this.allocations.push( this.buildAllocationItem(alo, this.positions, rates));
    }
    this.allocations
    .sort((a,b) => (b.totalValuation - a.totalValuation))
    .sort((a,b) => (b.percentage - a.percentage));

    this.bucketValuation = this.allocations.reduce(((total,p) => total + p.totalValuation), 0)
  }

  positionsPerTag(tag: string, positions: PositionWithValue[]) {
    return positions.filter(function (position) {
      return position.tags.map((t) => t.toUpperCase()).indexOf(tag.toUpperCase()) >= 0;
    });
  }

  updateChartData() {
    const tags = this.allocations.map((al) => al.tag);
    const newCurrentSeries = this.allocations.map((al) => 100 * al.totalValuation / this.bucketValuation);

    this.showExpectedChart = tags.length > 0;
    this.showCurrentChart = this.bucketValuation > 0 && newCurrentSeries.length > 0;

    this.currentDonutOptions.series = newCurrentSeries;
    this.currentDonutOptions.labels = tags;
    this.updateExpectedChartSeries();
    this.expectedPieOptions.labels = tags;
  }

  updateExpectedChartSeries() {
    this.expectedPieOptions.series = this.allocations.map((al) => al.percentage);
  }

  updateSumPerc() {
    this.missingPercentage = 100 - this.allocations.reduce(((total, al) => total + al.percentage), 0);
  }

  onAddAllocation() {
    this.allocationFormVisible = true;
  }

  onFormTrash() {
    this.allocationForm.reset();
    this.allocationFormVisible = false;
  }

  onFormKeyUp(e) {
    if (e.keyCode === 27) {
      this.onFormTrash();
    }
  }

  isPercentageValid(noErrWhenUntouched: boolean = true) {
    return (noErrWhenUntouched && !this.allocationForm.get("percentage").touched)
           || this.allocationForm.get("percentage").errors === null;
  }

  isTagValid(noErrWhenUntouched: boolean = true) {
    return (noErrWhenUntouched && !this.allocationForm.get("tag").touched)
            || this.allocationForm.get("tag").errors === null
  }

  onAllocationFormSubmit() {
    this.allocationForm.markAllAsTouched();

    if (!this.isPercentageValid(false) || !this.isTagValid(false))
    {
      return;
    }
  
    const alo = this.allocationFormToBucketAllocation();
    bucketService.addAllocation(this.bucket, alo);

    this.tagsChanged();

    this.allocationForm.reset();
    if (this.missingPercentage === 0) {
      this.allocationFormVisible = false;
    }
  }

  allocationFormToBucketAllocation() : BucketAllocation {
    return {
      tag: this.allocationForm.value.tag,
      percentage: +this.allocationForm.value.percentage
    }
  }

  onNewPercentageChange(ev: any) {
    this.missingPercentage = 100 - this.allocations.reduce(((total, al) => total + al.percentage), 0) - +this.allocationForm.value.percentage;
  }

  buildAllocationItem(alo, positions: PositionWithValue[], rates: ExchangeRate[]) {
    const relPos = this.positionsPerTag(alo.tag, positions);
    var aloc = <AllocationItem>{
      totalValuation: relPos.reduce(((total,p) => total + this.positionToValue(p, rates).value), 0),
      relevantPositions: relPos.map((rp) => ({name: rp.name, value: this.positionToValue(rp, rates)})).sort((a,b) =>
        b.value.value - a.value.value),
      ...alo
    }
    aloc.anythingOutdated = aloc.relevantPositions.some((v) => v.value.outdated);

    return aloc;
  }

  positionToValue(p: PositionWithValue, rates: ExchangeRate[]) {
    if (!p.lastMarketValue) {
      return { value: 0, outdated: false };
    }

    const myRate = rates.find((v) => v && v.fromCurrencyId == p.lastMarketValue.currencyId && v.toCurrencyId == this.mainCurrency.id);

    if (!myRate) {
      return { value: p.lastMarketValue.value * p.currentQuantity, outdated: false };
    }

    const somethingStale: boolean =
      getDifferenceInDays(moment().valueOf(), moment.utc(p.lastMarketValue.date).valueOf()) > this.staleDays ||
      Math.abs(getDifferenceInDays(p.lastMarketValue.date, myRate.date)) > this.staleDays;
    return { value: p.lastMarketValue.value * myRate.rate * p.currentQuantity, outdated: somethingStale };
  }

  changeBucketName(name: string) {
    bucketService.changeBucketName(this.bucket, name);
  }

  changeBucketAllocation(alloc: AllocationItem, perc: number) {
    bucketService.changeAllocation(this.bucket, <BucketAllocation>{tag: alloc.tag, percentage: alloc.percentage}, perc);
    this.allocations[this.allocations.indexOf(alloc)].percentage = perc;
    this.updateExpectedChartSeries();
  }
  allocationPercenntageChanged(alloc: AllocationItem, perc: number) {
    this.updateSumPerc();
    this.missingPercentage = this.missingPercentage + alloc.percentage - perc;
  }

  bucketTrashClicked() {
    this.onDelete.emit(this.bucket);
  }

  allocationTrashClicked(alloc: AllocationItem) {
    bucketService.deleteAllocation(this.bucket, alloc);
    this.tagsChanged();
  }
}
