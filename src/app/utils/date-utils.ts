import * as moment from "moment";

export function formatDateTimeToLocal(dt: number): string
{
    var out: string;
    const local = moment.utc(dt).local();
    out = local.year().toString().substring(2, 4) + '-';
    out += (local.month() + 1).toString().padStart(2, '0') + '-';
    out += local.date().toString().padStart(2, '0');

    out += ' ';

    out += local.hour().toString().padStart(2, '0') + ':';
    out += local.minute().toString().padStart(2, '0') + ':';
    out += local.second().toString().padStart(2, '0');
    return out;
}

export function getDifferenceInDays(dta: number, dtb: number) : number {
    return (dta - dtb)
    / (1000 * 60 * 60 * 24)
}