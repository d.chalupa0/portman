import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageNotFoundComponent } from './shared/components';

import { SettingsRoutingModule } from './settings/settings-routing.module';
import { PositionsRoutingModule } from './positions/positions-routing.module';
import { TransactionsRoutingModule } from './transactions/transactions-routing.module';
import { BucketsRoutingModule } from './buckets/buckets-routing.module';
import { ExchangeRatesRoutingModule } from './exchange-rates/exchange-rates-routing.module';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'positions',
    pathMatch: 'full'
  },
  {
    path: '**',
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { relativeLinkResolution: 'legacy' }),
    SettingsRoutingModule,
    PositionsRoutingModule,
    TransactionsRoutingModule,
    BucketsRoutingModule,
    ExchangeRatesRoutingModule
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
