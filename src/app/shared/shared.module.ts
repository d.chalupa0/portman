import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from '@ngx-translate/core';

import { PageNotFoundComponent } from './components/';
import { WebviewDirective } from './directives/';
import { FormsModule } from '@angular/forms';
import { InPlaceInputComponent } from './components/in-place-input/in-place-input.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

@NgModule({
  declarations: [PageNotFoundComponent, WebviewDirective, InPlaceInputComponent],
  imports: [CommonModule, TranslateModule, FormsModule, MatInputModule, MatFormFieldModule],
  exports: [TranslateModule, WebviewDirective, FormsModule, InPlaceInputComponent]
})
export class SharedModule {}
