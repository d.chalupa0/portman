import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InPlaceInputComponent } from './in-place-input.component';

describe('InPlaceInputComponent', () => {
  let component: InPlaceInputComponent;
  let fixture: ComponentFixture<InPlaceInputComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InPlaceInputComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InPlaceInputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
