import { ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-in-place-input',
  templateUrl: './in-place-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./in-place-input.component.scss']
})
export class InPlaceInputComponent<T> implements OnInit, OnChanges {
  @Input() value: T
  @Input() label?: string
  @Input() required: boolean = false;
  @Input() format: string = "1.2-2";
  @Output() onChange: EventEmitter<T> = new EventEmitter<T>()
  @Output() onSubmit: EventEmitter<T> = new EventEmitter<T>()
  valueInputElement!: ElementRef<HTMLInputElement>;

  @ViewChild('valueInput') set inputElRef(elRef: ElementRef<HTMLInputElement>) {
    if (elRef) {
      this.valueInputElement = elRef;
      this.valueInputElement.nativeElement.focus();
    }
  };

  inputVisible: boolean = false;
  changedValue: any;

  constructor() { }

  ngOnInit(): void {
    this.changedValue = this.value;
  }

  getTypeName(): string {
    return this.value?.constructor.name.toLowerCase();
  }

  onClicked(): void {
    this.inputVisible = true;
  }

  submitted() {
    if (!this.isValueValid()) {
      return;
    }

    this.onSubmit.emit(this.changedValue);
    this.inputVisible = false;
  }

  changed() {
    this.onChange.emit(this.changedValue);
  }

  onBlur() {
    this.changedValue = this.value;
    this.inputVisible = false;
  }

  onKeyUp(e) {
    if(e.keyCode === 13) {
      this.submitted();
    } else if (e.keyCode === 27) {
      this.onBlur();
    } else {
      this.changed();
    }
  }

  isValueValid(): boolean {
    return !this.required || ( this.changedValue != null && this.changedValue != undefined && this.changedValue !== "" );
  }

  ngOnChanges(changes: SimpleChanges) {
    this.value = changes.value.currentValue;
    this.changedValue = this.value;
  }
}