import { Injectable } from '@angular/core';
import { MarketValue, db } from '../../db';
import { liveQuery } from 'dexie';

@Injectable({
  providedIn: 'root'
})
export class MarketValueService {

  constructor() { }

  observeLastValuesForPosition(positionId: number, count: number) {
    return liveQuery(() => this.getLastValuesForPosition(positionId, count));
  }

  getLastValuesForPosition(positionId: number, count: number) {
    return db.marketValueHistory
    .orderBy('date').reverse()
    .filter(function (mv) {
      return mv.positionId === positionId; })
    .limit(count)
    .toArray();
  }

  addMarketValue(mv: MarketValue) {
    db.marketValueHistory.add(mv);
  }
}

export const marketValueService = new MarketValueService();