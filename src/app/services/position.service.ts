import { Injectable } from '@angular/core';
import { Position, db, Transaction, MarketValue } from '../../db';
import { liveQuery } from 'dexie';
import { transactionService } from './transaction.service';
import { marketValueService } from './market-value.service';
import { from } from 'rxjs';

export interface PositionWithValue extends Position {
  lastMarketValue?: MarketValue
}

@Injectable({
  providedIn: 'root'
})
export class PositionService {

  constructor() {
    transactionService.transactionAdded.subscribe((t) => this.processTransaction(t))
  }

  getActivePositions() {
    return liveQuery(() => db.positions.where({closed: 0}).toArray());
  }

  addPosition(pos: Position) {
    db.positions.add(pos);
  }

  updatePosition(pos: Position) {
    db.positions.update(pos.id, pos);
  }

  getPosition(id: number) {
    return db.positions.filter(function (p) {
      return p.id === id; }).first();
  }

  observePosition(id: number) {
    return liveQuery(() => this.getPosition(id));
  }

  observePositionsByTags(tags: string[]) {
    return liveQuery(() => this.getPositionsByTags(tags));
  }

  async getPositionsByTags(tags: string[]) {
    const positions = await db.positions.filter(function (p) {
      var upperTags = tags.map((t) => t.toUpperCase());
      var upperPTags = p.tags.map((t_1) => t_1.toUpperCase());
      let common = upperTags.filter(tag => upperPTags.indexOf(tag) >= 0);
      return p.closed === 0 && common.length > 0;
    }).toArray();
    return await Promise.all(positions.map(async function (p_1) {
      return marketValueService.getLastValuesForPosition(p_1.id, 1)
        .then((value) => (<PositionWithValue>{ lastMarketValue: value[0], ...p_1 }));
    }));
  }

  processTransaction(t: Transaction) {
    db.transaction("rw", db.positions, async () => {
      let [from, to] = await Promise.all([
        t.fromId ? db.positions.where("id").equals(t.fromId).first() : undefined,
        t.toId ? db.positions.where("id").equals(t.toId).first() : undefined
      ]);

      if (from) {
        const closed: number = from.currentQuantity <= t.fromQty ? 1 : 0;
        db.positions.update(t.fromId, { currentQuantity: from.currentQuantity - t.fromQty, closed: closed});
      }

      if (to) {
        const closed: number = t.toQty <= 0 ? 1 : 0;
        db.positions.update(t.toId, { currentQuantity: to.currentQuantity + t.toQty, closed: closed});
      }
  })
  }

  async getUniqueTags() {
    const pos = await db.positions.toArray();
    const tags = pos.reduce(((fin, p) => fin.concat(p.tags)), <string[]>[]);
    return Array.from(new Set(tags));
  }
}

export const positionService = new PositionService();