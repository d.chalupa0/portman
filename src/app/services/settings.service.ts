import { Injectable } from '@angular/core';
import { db } from '../../db';
import { liveQuery } from 'dexie';
import { Settings } from '../../db/settings';
import { Currency } from '../../db/exchange';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  constructor() { }

  observeSettings() {
    return liveQuery(() => this.getSettings());
  }

  getSettings() {
    return db.settings.get(1);
  }

  setSettings(s: Settings) {
    db.settings.put(s, 1);
  }

  observeMainCurrency() {
    return liveQuery(() => this.getMainCurrency());
  }

  getMainCurrency() {
    return db.settings.get(1)
    .then((s) => (s.mainCurrencyId ? db.currencies.get(s.mainCurrencyId) : null))
  }

  observeStaleDayCount() {
    return liveQuery(() => this.getStaleDayCount());
  }

  getStaleDayCount() {
    return db.settings.get(1)
    .then((s) => s.staleDayCount)
  }

  observeExchangeRateApi() {
    return liveQuery(() => this.getExchangeRateApi());
  }

  getExchangeRateApi() {
    return db.settings.get(1)
    .then((s) => s.exchangeRateApi)
  }
}

export const settingsService = new SettingsService;