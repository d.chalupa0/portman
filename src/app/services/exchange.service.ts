import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { liveQuery } from 'dexie';
import { db } from '../../db';
import { Currency, ExchangeRate } from '../../db/exchange';
import { ExchangeRateApiType } from '../../db/settings';
import { AbstractApi } from './exchange-apis/abstract';
import { CommonExchangeRateApi, ExchangeRateApiReturn } from './exchange-apis/common-exchange-rate-api';
import { settingsService } from './settings.service';

@Injectable({
  providedIn: 'root'
})
export class ExchangeService {
  private myApi: CommonExchangeRateApi;

  constructor(private httpClient: HttpClient) {
    settingsService.observeExchangeRateApi().subscribe((api) => this.exchangeApiChanged(api));
  }

  exchangeApiChanged(apiSettings) {
    switch (apiSettings?.api) {
      case ExchangeRateApiType.ABSTRACT_API:
        this.myApi = new AbstractApi(apiSettings, this.httpClient);
        console.log('Using Abstract API for exchange rates');
        break;

      default:
        this.myApi = null;
        console.log('No exchange rate API set');
      break;
    }
  }

  getCurrency(currencyId: number) {
    return db.currencies.where("id").equals(currencyId).first()
  }

  getAllCurrencies() {
    return db.currencies.toArray();
  }

  observeAllCurrencies() {
    return liveQuery(() => this.getAllCurrencies());
  }

  addCurrency(c: Currency) {
    return db.currencies.add(c);
  }

  observeLatestRate(sourceId: number, targetId: number) {
    return liveQuery(() => db.exchangeRates
    .orderBy('date').reverse()
    .filter(function (rate) {
      return (rate.fromCurrencyId === sourceId && rate.toCurrencyId === targetId) || (rate.fromCurrencyId === targetId && rate.toCurrencyId === sourceId); })
    .first()
    .then(function (rate) {
      if (rate && rate.fromCurrencyId === targetId) {
        return {
          ...rate,
          fromCurrencyId: rate.toCurrencyId,
          toCurrencyId: rate.fromCurrencyId,
          rate: 1/rate.rate
        };
      }
      return rate;
    })
    )
  }

  observeClosestRate(sourceId: number, targetId: number, date: number)
  {
    return liveQuery(() => this.getClosestRate(sourceId, targetId, date));
  }

  getClosestRate(sourceId: number, targetId: number, date: number) {
    return db.exchangeRates
    .filter(function (rate) {
      return (rate.fromCurrencyId === sourceId && rate.toCurrencyId === targetId) || (rate.fromCurrencyId === targetId && rate.toCurrencyId === sourceId); })
    .toArray()
    .then(function (rates) {
      rates.sort((ra, rb) => Math.abs(date - ra.date) - Math.abs(date - rb.date));
      if (rates[0] && rates[0].fromCurrencyId === targetId) {
        return {
          ...rates[0],
          fromCurrencyId: rates[0].toCurrencyId,
          toCurrencyId: rates[0].fromCurrencyId,
          rate: 1/rates[0].rate
        };
      }
      return rates[0];
    })
  }

  addRate(rate: ExchangeRate) {
    db.exchangeRates.add(rate);
  }

  retrieveApiRates(base: string, target: string[]) {
    if (!this.myApi) {
      alert(`Setup API First`);
      return Promise.reject<ExchangeRateApiReturn[]>("API not setup");
    }
    return this.myApi.getRates(base, target);
  }
}