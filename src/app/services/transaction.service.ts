import { Injectable, EventEmitter } from '@angular/core';
import { Transaction, db } from '../../db';
import { liveQuery } from 'dexie';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {
  transactionAdded: EventEmitter<Transaction> = new EventEmitter()

  constructor() { }

  getTransactions() {
    return liveQuery(() => db.transactionHistory.orderBy('date').reverse().toArray());
  }

  observeLatestTransaction(positionId: number) {
    return liveQuery(() => this.getLatestTransaction(positionId));
  }

  getLatestTransaction(positionId: number) {
    return db.transactionHistory
           .orderBy('date').reverse()
           .filter(function (t) {
             return t.fromId === positionId || t.toId === positionId; })
           .limit(1)
           .first();
  }

  addTransaction(t: Transaction) {
    db.transactionHistory.add(t)
    .then(() => this.transactionAdded.emit(t));
  }
}

export const transactionService = new TransactionService();