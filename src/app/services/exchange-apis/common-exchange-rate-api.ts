import { ExchangeRateApiType, ExchangeRateApi } from "../../../db/settings";

export interface ExchangeRateApiReturn {
    fromCurrency: string,
    toCurrency: string,
    date: number,
    rate: number
}

export abstract class CommonExchangeRateApi {
    name: ExchangeRateApiType;

    constructor(protected apiSettings: ExchangeRateApi) {
        this.name = apiSettings.api
    }
    abstract getRates(baseCurrency: string, targetCurrencies: string[]): Promise<ExchangeRateApiReturn[]>;
}