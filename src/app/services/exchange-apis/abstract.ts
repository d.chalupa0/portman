import { HttpClient } from "@angular/common/http";
import { firstValueFrom } from "rxjs";
import { ExchangeRateApi } from "../../../db/settings";
import { CommonExchangeRateApi, ExchangeRateApiReturn } from "./common-exchange-rate-api";

interface AbstractApiExchange {
    base: string,
    exchange_rates: {[key: string]: number},
    last_updated: number
}

export class AbstractApi extends CommonExchangeRateApi {
    constructor(protected apiSettings: ExchangeRateApi, private httpClient: HttpClient) { super(apiSettings); }

    getRates(baseCurrency: string, targetCurrencies: string[]): Promise<ExchangeRateApiReturn[]> {
        return firstValueFrom(
            this.httpClient.get<AbstractApiExchange>(`https://exchange-rates.abstractapi.com/v1/live?api_key=${this.apiSettings.key}&base=${baseCurrency}`))
        .then(function (r) {
            var out = []
            for (const rate in r.exchange_rates) {
                if (targetCurrencies.includes(rate)) {
                    out.push({
                        fromCurrency: r.base,
                        toCurrency: rate,
                        date: new Date(r.last_updated*1000),
                        rate: r.exchange_rates[rate]
                    })
                }
            }
            return out;
        });
    }
}