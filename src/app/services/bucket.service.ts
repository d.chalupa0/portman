import { Injectable } from '@angular/core';
import { PortfolioBucket, db, BucketAllocation } from '../../db';
import { liveQuery } from 'dexie';

@Injectable({
  providedIn: 'root'
})
export class BucketService {
  constructor() { }

  getBuckets() {
    return db.portfolioBuckets.toArray();
  }

  addBucket(b: PortfolioBucket) {
    return db.portfolioBuckets.add(b);
  }

  addAllocation(b: PortfolioBucket, alloc: BucketAllocation) {
    b.allocations.push(alloc);
    db.portfolioBuckets.update(b.id,{allocations: b.allocations});
  }

  changeBucketName(b: PortfolioBucket, name: string) {
    b.name = name;
    db.portfolioBuckets.update(b.id,{name: b.name});
  }

  changeAllocation(b: PortfolioBucket, alloc: BucketAllocation, perc: number) {
    b.allocations[b.allocations.findIndex((a) => a.tag === alloc.tag)].percentage = perc;
    db.portfolioBuckets.update(b.id, {allocations: b.allocations});
  }

  deleteBucket(b: PortfolioBucket) {
    db.portfolioBuckets.delete(b.id);
  }

  deleteAllocation(b: PortfolioBucket, alloc: BucketAllocation) {
    b.allocations = b.allocations.filter((bucket) => bucket.tag !== alloc.tag);
    db.portfolioBuckets.update(b.id, {allocations: b.allocations});
  }

  getUniqueTags() {
    return db.portfolioBuckets.toArray()
    .then((pos) => pos.reduce(((fin, p) => fin.concat(p.allocations.map((a) => a.tag))), <string[]>[]))
    .then((tags) => Array.from(new Set(tags)));
  }
}

export const bucketService = new BucketService();