import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Currency } from '../../db/exchange';
import { exchangeRateApiList, ExchangeRateApiType, Settings } from '../../db/settings';
import { ExchangeService } from '../services/exchange.service';
import { settingsService } from '../services/settings.service';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {

  settingsForm: FormGroup;

  oldSettings: Settings;
  currencyList: Currency[];
  exchangeApis: ExchangeRateApiType[] = exchangeRateApiList;

  constructor(private fb: FormBuilder, private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    this.settingsForm = this.fb.group({
      mainCurrencyId: [1, [Validators.required]],
      apiType: [0, [Validators.required]],
      apiKey: [null],
      staleDays: [14]
    });

    settingsService.observeSettings()
    .subscribe(s => this.initForm(s));

    this.exchangeService.observeAllCurrencies()
    .subscribe((cs) =>
      this.currencyList = cs);
  }

  get mainCurrencyId() { return this.settingsForm.get('mainCurrencyId'); }
  get apiType() { return this.settingsForm.get('apiType'); }
  get apiKey() { return this.settingsForm.get('apiKey'); }
  get staleDays() { return this.settingsForm.get('staleDays'); }

  initForm(s: Settings) {
    this.oldSettings = s;
    this.mainCurrencyId.setValue(s.mainCurrencyId);
    this.apiType.setValue(s.exchangeRateApi?.api);
    this.apiKey.setValue(s.exchangeRateApi?.key);
    this.staleDays.setValue(s.staleDayCount);
  }

  onSubmit() {
    settingsService.setSettings({
      id: 1,
      mainCurrencyId: +this.mainCurrencyId.value,
      exchangeRateApi: {
        api: this.apiType.value,
        key: this.apiKey.value
      },
      staleDayCount: this.staleDays.value
    });
  }
}
