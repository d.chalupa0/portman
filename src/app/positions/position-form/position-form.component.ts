import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { Position } from '../../../db';
import { positionService } from '../../services/position.service';
import { ActivatedRoute, Router } from '@angular/router';
import { bucketService } from '../../services/bucket.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import {COMMA, ENTER} from '@angular/cdk/keycodes';
import {MatAutocompleteSelectedEvent} from '@angular/material/autocomplete';
import {MatChipInputEvent} from '@angular/material/chips';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';

@Component({
  selector: 'app-position-form',
  templateUrl: './position-form.component.html',
  styleUrls: ['./position-form.component.scss']
})
export class PositionFormComponent implements OnInit {

  @ViewChild('positionTags') positionTagsInput: ElementRef<HTMLInputElement>;
  oldPosition: Position = {
    name: "",
    currentQuantity: 0,
    closed: 0,
    tags: []
  };

  separatorKeysCodes: number[] = [ENTER, COMMA];
  filteredTags: Observable<string[]>;
  tags: string[] = [];
  availableTags: string[] = [];
  positionForm: FormGroup;

  constructor(private fb: FormBuilder, private router: Router, private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.positionForm = this.fb.group({
      positionName: [null, [Validators.required]],
      positionTags: [this.tags, []]
    });

    this.filteredTags = this.positionTags.valueChanges.pipe(
      startWith(null),
      map((tag: string | null) => (tag ? this._filter(tag) : this.availableTags.slice())),
    );

    if (this.route.snapshot.paramMap.has('id')) {
      positionService.observePosition(+this.route.snapshot.paramMap.get('id')).subscribe((pos) =>
      (
        this.positionName.setValue(pos.name),
        this.oldPosition = pos,
        this.retrieveAvailableTags()
      ));
    } else {
      this.retrieveAvailableTags();
    }
  }

  onSubmit()
  {
    this.oldPosition.tags = this.tags;
    this.oldPosition.name = this.positionName.value;
    if (this.oldPosition.id) {
      positionService.updatePosition(this.oldPosition);
    } else {
      positionService.addPosition(this.oldPosition);
    }
    this.router.navigate(['/positions']);
  }

  retrieveAvailableTags() {
    Promise.all([positionService.getUniqueTags(), bucketService.getUniqueTags()])
    .then(([a, b]) => (
      this.availableTags = Array.from(new Set(a.concat(b))),
      this.tags = this.availableTags.filter((t) => this.oldPosition.tags.includes(t)),
      this.positionTags.setValue('')
    ));
  }


  onAdd(event: MatChipInputEvent) {
    // The input and the autocomplete have a race condition
    setTimeout(() => {
      this.add(event)
    }, 200);
  }

  add(event: MatChipInputEvent): void {
    const value = (event.value || '').trim();

    if (this.positionTags.value) {
      this.tags.push(this.positionTags.value);
      this.positionTagsInput.nativeElement.value = '';
    }

    event.chipInput!.clear();

    this.positionTags.setValue(null);
  }

  remove(tag: string): void {
    const index = this.tags.indexOf(tag);

    if (index >= 0) {
      this.tags.splice(index, 1);
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tags.push(event.option.viewValue);
    this.positionTagsInput.nativeElement.value = '';
    this.positionTags.setValue(null);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.availableTags.filter(tag => tag.toLowerCase().includes(filterValue));
  }

  isNameValid() {
    return this.positionName.pristine || this.positionName.valid;
  }
  isFormValid() {
    return this.positionName.valid && this.positionTags.valid;
  }
  get positionName() { return this.positionForm.get('positionName'); }
  get positionTags() { return this.positionForm.get('positionTags'); }
}
