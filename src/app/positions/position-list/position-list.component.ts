import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { MarketValue, Position } from '../../../db';
import { positionService } from '../../services/position.service';
import { marketValueService } from '../../services/market-value.service';

import { transactionService } from '../../services/transaction.service';

@Component({
  selector: 'app-position-list',
  templateUrl: './position-list.component.html',
  styleUrls: ['./position-list.component.scss']
})
export class PositionListComponent implements OnInit {
  @Output() positionsChanged: EventEmitter<Position[]> = new EventEmitter();
  positions: Position[];

  constructor() { }

  ngOnInit(): void {
    positionService.getActivePositions().subscribe((positions) => (
      this.positions = positions,
      this.positionsChanged.emit(this.positions)
    ));
  }

  addMarketValueChange(mv: MarketValue)
  {
    marketValueService.addMarketValue(mv);
  }
}
