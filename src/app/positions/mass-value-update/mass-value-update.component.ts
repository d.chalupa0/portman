import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { combineLatest } from 'rxjs';
import { MarketValue, Position } from '../../../db';
import { marketValueService } from '../../services/market-value.service';
import { positionService } from '../../services/position.service';
import { settingsService } from '../../services/settings.service';
import { MassValueUpdateItemComponent } from '../mass-value-update-item/mass-value-update-item.component';

@Component({
  selector: 'app-mass-value-update',
  templateUrl: './mass-value-update.component.html',
  styleUrls: ['./mass-value-update.component.scss']
})
export class MassValueUpdateComponent implements OnInit {
  @ViewChildren('value') itemComponents: QueryList<MassValueUpdateItemComponent>;

  positions: Position[];
  newMarketValues: MarketValue[];
  finished = 0;
  outdatedDays: number;

  constructor() { }

  ngOnInit(): void {
    combineLatest([
      positionService.getActivePositions(),
      settingsService.observeStaleDayCount()
    ])
    .subscribe(([positions, staleDays]) => (
      this.positions = positions,
      this.outdatedDays = staleDays))
  }

  onFinalize() {
    this.itemComponents.map(function (item) {
      const newValue = item.buildNewMarketValue();
      if (newValue) {
        marketValueService.addMarketValue(newValue);
      }
    });
  }

  onValidityChanged(e: boolean) {
    this.finished += e ? 1 : -1;
  }
}
