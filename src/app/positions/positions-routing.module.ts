import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MassValueUpdateComponent } from './mass-value-update/mass-value-update.component';
import { PositionFormComponent } from './position-form/position-form.component';
import { PositionsComponent } from './positions.component';

const routes: Routes = [
  {
    path: 'positions',
    component: PositionsComponent
  },
  {
    path: 'positions/new',
    component: PositionFormComponent
  },
  {
    path: 'positions/edit/:id',
    component: PositionFormComponent,
  },
  {
    path: 'positions/massupdate',
    component: MassValueUpdateComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PositionsRoutingModule { }
