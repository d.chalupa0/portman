import { Component, OnInit } from '@angular/core';
import { Position } from '../../db';

@Component({
  selector: 'app-positions',
  templateUrl: './positions.component.html',
  styleUrls: ['./positions.component.scss']
})
export class PositionsComponent implements OnInit {

  hasPositions: boolean = false;
  constructor() { }

  ngOnInit(): void {
  }

  onPositionsChanged(positions: Position[]) {
    this.hasPositions = !positions || positions.length > 0;
  }

}
