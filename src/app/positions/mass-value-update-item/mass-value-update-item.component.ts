import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MarketValue, Position } from '../../../db';
import { Currency, ExchangeRate } from '../../../db/exchange';
import { ExchangeService } from '../../services/exchange.service';
import { marketValueService } from '../../services/market-value.service';
import { formatDateTimeToLocal, getDifferenceInDays } from '../../utils/date-utils';

import { settingsService } from '../../services/settings.service';
import * as moment from 'moment';

@Component({
  selector: 'app-mass-value-update-item',
  templateUrl: './mass-value-update-item.component.html',
  styleUrls: ['./mass-value-update-item.component.scss']
})
export class MassValueUpdateItemComponent implements OnInit {
  @Input() position: Position;
  @Input() outdatedDays: number;
  @Output() validityChanged: EventEmitter<boolean> = new EventEmitter();

  lastMarket: MarketValue;
  lastMarketDateString: string;
  lastCurrency: Currency;
  currencyList: Currency[];
  selectedCurrency: Currency;
  selectedExchangeRate: ExchangeRate;
  selectedExchangeDateString: string;
  exchangeRateSubscription: any;
  exchangeOutdated: boolean;

  newMarketForm: FormGroup;

  readonly warningIcon: string = "warning";
  readonly checkIcon: string = "check_circle";
  readonly questionIcon: string = "help";
  icon: string = this.questionIcon;
  message: string;

  percentageChange: number;

  constructor(private fb: FormBuilder, private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    this.newMarketForm = this.fb.group({
      value: [null, [Validators.required]],
      currencyId: [1, [Validators.required]],
      date: moment().toDate()
    });

    this.currencyId.valueChanges.subscribe(() => this.onCurrencyChange());
    this.value.valueChanges.subscribe(() => (this.recalculatePercentage()));
    this.date.valueChanges.subscribe(() => (this.updateLastRate()));

    this.newMarketForm.statusChanges.subscribe(() => this.updateStatus());
    this.exchangeService.observeAllCurrencies()
    .subscribe((cs) => (
      this.currencyList = cs,
      this.updateSelectedCurrency(),
      this.updateLastCurrency()));

    marketValueService.observeLastValuesForPosition(this.position.id, 1)
    .subscribe((m) => (
      this.lastMarket = m[0],
      this.lastMarketUpdated()));
  }

  lastMarketUpdated() {
    if (!this.lastMarket) {
      return;
    }

    this.lastMarketDateString = formatDateTimeToLocal(this.lastMarket?.date),
    this.updateLastCurrency(),
    this.initNewMarket(),
    this.updateLastRate()
  }

  onCurrencyChange() {
    this.selectedExchangeRate = null,
    this.updateSelectedCurrency(),
    this.updateLastRate(),
    this.updateStatus()
  }

  recalculatePercentage() {
    if (this.value.value === null || !this.lastMarket) {
      this.percentageChange = null;
      return;
    }

    var oldValue = this.lastMarket.value;
    var newValue = +this.value.value;

    if (this.selectedCurrency.id !== this.lastCurrency.id && this.selectedExchangeRate)
    {
      if (this.selectedExchangeRate) {
        oldValue *= this.selectedExchangeRate.rate;
      } else {
        this.percentageChange = null;
        return;
      }
    }

    this.percentageChange = 100 * (newValue - oldValue) / oldValue;
  }

  initNewMarket() {
    if (this.newMarketForm.touched) {
      return;
    }

    this.currencyId.setValue(this.lastMarket.currencyId);
  }

  updateLastCurrency() {
    if (this.lastMarket && this.currencyList) {
      this.lastCurrency = this.currencyList.find((c) => c.id === this.lastMarket.currencyId);
    }
  }

  updateSelectedCurrency() {
    if (this.currencyList) {
      this.selectedCurrency = this.currencyList.find((c) => c.id === +this.currencyId.value);
    }
  }

  updateLastRate() {
    if (!this.lastMarket) {
      return;
    }

    if (this.exchangeRateSubscription) {
      this.exchangeRateSubscription.unsubscribe();
    }

    this.exchangeRateSubscription = this.exchangeService.observeClosestRate(
      this.lastMarket.currencyId,
      +this.currencyId.value,
      moment(this.date.value).valueOf())
    .subscribe((rate) => (
      this.selectedExchangeRate = rate,
      this.rateUpdated()
    ));
  }

  rateUpdated() {
    if (this.selectedExchangeRate) {
      this.exchangeOutdated = Math.abs(getDifferenceInDays(moment.utc(this.selectedExchangeRate.date).valueOf(), this.date.value)) > this.outdatedDays,
      this.selectedExchangeDateString = formatDateTimeToLocal(this.selectedExchangeRate.date)
    } else {
      this.exchangeOutdated = false;
    }
    this.updateStatus();
    this.recalculatePercentage();
  }

  updateStatus() {
    const before = this.icon;
    if (this.newMarketForm.pristine) {
      this.message = '';
      this.icon = this.questionIcon;
    } else if ((this.value.invalid && (this.value.dirty || this.value.touched))
        || (this.currencyId.invalid && (this.currencyId.dirty || this.currencyId.touched))
        || (this.date.invalid && (this.date.dirty || this.date.touched))) {
          this.message = 'ERRORS.INVALID_DATA';
          this.icon = this.warningIcon;
    } else if (this.value.invalid || this.currencyId.invalid || this.date.invalid) {
      this.message = '';
      this.icon = this.questionIcon;
    } else {
      this.message = '';
      this.icon = this.checkIcon;
    }

    if (this.icon !== before) {
      this.validityChanged.emit(this.icon === this.checkIcon);
    }
  }

  buildNewMarketValue() : MarketValue | null {
    if (this.icon !== this.checkIcon) {
      return null;
    }

    return {
      positionId: this.position.id,
      currencyId: this.selectedCurrency.id,
      value: +this.value.value,
      date: moment(this.date.value).valueOf(),
    }
  }

  get value() { return this.newMarketForm.get('value'); }
  get currencyId() { return this.newMarketForm.get('currencyId'); }
  get date() { return this.newMarketForm.get('date'); }
}
