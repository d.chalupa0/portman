import { Component, OnInit, Input, Output, EventEmitter, ViewChild, ElementRef } from '@angular/core';
import { Position, MarketValue, Transaction } from '../../../db';
import { combineLatest, mergeMap, Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import { settingsService } from '../../services/settings.service';
import { formatDateTimeToLocal, getDifferenceInDays } from '../../utils/date-utils';
import { Currency, ExchangeRate } from '../../../db/exchange';
import { transactionService } from '../../services/transaction.service';
import { marketValueService } from '../../services/market-value.service';
import { ExchangeService } from '../../services/exchange.service';
import * as moment from 'moment';

@Component({
  selector: 'app-position-item',
  templateUrl: './position-item.component.html',
  styleUrls: ['./position-item.component.scss']
})
export class PositionItemComponent implements OnInit {
  @Input() position: Position
  @Output() onMarketValueChange: EventEmitter<MarketValue> = new EventEmitter()

  mainCurrency: Currency;

  currentMarketValue: MarketValue;
  currentRate: ExchangeRate;
  currentRateDate: string;
  currentCurrency: Currency;
  currentDateFormatted: string = '';

  lastMarketValue: MarketValue;
  lastRate: ExchangeRate;
  lastRateDate: string;
  lastCurrency: Currency;
  lastDateFormatted: string = '';

  hasLastValue: boolean;
  valueChangePerc: number;
  hasLastQty: boolean;
  qtyChangePerc: number;
  currentItemValueInMainCurrency: number;
  lastItemValueInMainCurrency: number;

  marketValueStale: boolean = false;
  lastRateStale: boolean = false;
  currentRateStale: boolean = false;

  staleDays: number;

  constructor(private router : Router, private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    const mainSub = combineLatest([ settingsService.observeMainCurrency(),
      marketValueService.observeLastValuesForPosition(this.position.id, 2),
      transactionService.observeLatestTransaction(this.position.id),
      settingsService.observeStaleDayCount() ])

    const currencySub = mainSub.pipe(
        mergeMap(([currency, values, transaction, staleDays]) => (
            this.mainCurrency = currency,
            this.staleDays = staleDays,
            this.setupLastQty(transaction),
            this.setupLastValue(values),
            combineLatest([
              this.lastMarketValue ?
                this.exchangeService.observeClosestRate(this.lastMarketValue.currencyId, this.mainCurrency.id, this.lastMarketValue.date)
                : of(<ExchangeRate>undefined),
              this.exchangeService.observeClosestRate(this.currentMarketValue.currencyId, this.mainCurrency.id, this.currentMarketValue.date),
              this.lastMarketValue ?
                this.exchangeService.getCurrency(this.lastMarketValue.currencyId)
                : of(<Currency>undefined),
              this.exchangeService.getCurrency(this.currentMarketValue.currencyId) ]))),

      ).subscribe(([lastRate, currentRate, lastCurrency, currentCurrency]) => (
        this.lastRate = lastRate,
        this.lastRateDate = this.lastRate ? formatDateTimeToLocal(this.lastRate.date) : '',
        this.lastRateStale = this.lastRate ? Math.abs(getDifferenceInDays(this.lastMarketValue.date.valueOf(), this.lastRate.date.valueOf() )) > this.staleDays : false,
        this.lastCurrency = lastCurrency,
        this.currentRate = currentRate,
        this.currentRateStale = this.currentRate ? Math.abs(getDifferenceInDays(this.currentMarketValue.date.valueOf(), this.currentRate.date.valueOf() )) > this.staleDays : false,
        this.currentRateDate = this.currentRate ? formatDateTimeToLocal(this.currentRate.date) : '',
        this.currentCurrency = currentCurrency,
        this.recomputeItemValuesInMainCurrency()));
  }

  recomputeItemValuesInMainCurrency() {
    if (this.lastMarketValue?.currencyId === this.mainCurrency.id) {
      this.lastItemValueInMainCurrency = this.lastMarketValue.value;
    } else if (this.lastRate) {
      this.lastItemValueInMainCurrency = this.lastMarketValue.value / this.lastRate.rate;
    } else {
      this.lastItemValueInMainCurrency = null;
    }

    if (this.currentMarketValue.currencyId === this.mainCurrency.id) {
      this.currentItemValueInMainCurrency = this.currentMarketValue.value;
    } else if (this.currentRate) {
      this.currentItemValueInMainCurrency = this.currentMarketValue.value / this.currentRate.rate;
    } else {
      this.currentItemValueInMainCurrency = null;
    }

    if (this.hasLastValue) {
      this.valueChangePerc = (this.currentItemValueInMainCurrency / this.lastItemValueInMainCurrency - 1) * 100;
    }
  }

  setupLastValue(mvHistory: MarketValue[]) {
    if (mvHistory.length === 0) {
      return;
    }

    [this.currentMarketValue, this.lastMarketValue = undefined] = mvHistory;
  
    this.hasLastValue = this.lastMarketValue !== undefined;
    this.currentDateFormatted = formatDateTimeToLocal(this.currentMarketValue.date);
    this.marketValueStale = getDifferenceInDays(moment().valueOf(), moment.utc(this.currentMarketValue.date).valueOf()) > this.staleDays;

    if (this.lastMarketValue) {
      this.lastDateFormatted = formatDateTimeToLocal(this.lastMarketValue.date);
    } else {
      this.lastDateFormatted = '';
    }
  }

  setupLastQty(lastTransaction: Transaction) {
    if (lastTransaction == undefined) {
      return;
    }

    var lastQty: number = 0;

    if (lastTransaction.fromId === this.position.id) {
      lastQty = this.position.currentQuantity + lastTransaction.fromQty;
    } else if (lastTransaction.toId === this.position.id) {
      lastQty = this.position.currentQuantity - lastTransaction.toQty;
    } else {
      this.hasLastQty = false;
      return;
    }

    this.hasLastQty = lastQty !== 0;

    if (this.hasLastQty) {
      this.qtyChangePerc = (this.position.currentQuantity / lastQty - 1) * 100;
    }
  }

  onDelete(position: Position) {
    this.router.navigate(['/transactions/new/', {fromId: this.position.id, fromQty: this.position.currentQuantity}]);
  }

  onChangedValue(v) {
    settingsService.getMainCurrency()
    .then((c) =>
      this.onMarketValueChange.emit(
        {
          positionId: this.position.id,
          date: moment.now(),
          value: v,
          currencyId: c.id
        }
      )
    )
  }

  goToForm() {
    this.router.navigate([`/positions/edit/${this.position.id}`]);
  }

  goToSell() {
    this.router.navigate(['/transactions/new/', {fromId: this.position.id}]);
  }

  goToBuy() {
    this.router.navigate(['/transactions/new/', {toId: this.position.id}]);
  }
}
