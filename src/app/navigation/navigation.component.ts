import { Component, OnInit } from '@angular/core';
import { db, importDb } from '../../db';
import "dexie-export-import";
import { saveAs, FileSaverOptions } from 'file-saver'
import { HttpClient } from '@angular/common/http';
import Dexie from 'dexie';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
  }

  async onImport(event) {
    const file:File = event.target.files[0];

    if (file) {
      importDb(file);
    }
  }

  async onExport() {
    const blob = await db.export({prettyJson: true});
    saveAs(blob, "dexie-export.json");
  }
}
