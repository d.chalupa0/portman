import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';

import { AppRoutingModule } from './app-routing.module';

// NG Translate
import { TranslateModule, TranslateLoader, TranslatePipe } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { NgApexchartsModule} from "ng-apexcharts";

import { SettingsModule } from './settings/settings.module';

import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PositionsComponent } from './positions/positions.component';
import { PositionListComponent } from './positions/position-list/position-list.component';
import { PositionItemComponent } from './positions/position-item/position-item.component';
import { PositionFormComponent } from './positions/position-form/position-form.component';
import { TransactionListComponent } from './transactions/transaction-list/transaction-list.component';
import { TransactionItemComponent } from './transactions/transaction-item/transaction-item.component';
import { TransactionsComponent } from './transactions/transactions.component';
import { TransactionFormComponent } from './transactions/transaction-form/transaction-form.component';
import { BucketsComponent } from './buckets/buckets.component';
import { BucketItemComponent } from './buckets/bucket-item/bucket-item.component';
import { ExchangeRatesComponent } from './exchange-rates/exchange-rates.component';
import { ExchangeRateItemComponent } from './exchange-rates/exchange-rate-item/exchange-rate-item.component';
import { MassValueUpdateComponent } from './positions/mass-value-update/mass-value-update.component';
import { MassValueUpdateItemComponent } from './positions/mass-value-update-item/mass-value-update-item.component';
import { ExchangeService } from './services/exchange.service';
import { SettingsComponent } from './settings/settings.component';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatDividerModule } from '@angular/material/divider';
import { MatChipsModule } from '@angular/material/chips';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { NgxMatMomentModule } from '@angular-material-components/moment-adapter';
import { NgxMatDatetimePickerModule, NgxMatTimepickerModule } from '@angular-material-components/datetime-picker';

import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
// AoT requires an exported function for factories
const httpLoaderFactory = (http: HttpClient): TranslateHttpLoader =>  new TranslateHttpLoader(http, './assets/i18n/', '.json');

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    PositionsComponent,
    PositionListComponent,
    PositionItemComponent,
    PositionFormComponent,
    TransactionsComponent,
    TransactionListComponent,
    TransactionItemComponent,
    TransactionFormComponent,
    BucketsComponent,
    BucketItemComponent,
    ExchangeRatesComponent,
    ExchangeRateItemComponent,
    MassValueUpdateComponent,
    MassValueUpdateItemComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    TranslateModule.forRoot({
      defaultLanguage: 'en',
      loader: {
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    FormsModule,
    ReactiveFormsModule,
    NgApexchartsModule,
    MatFormFieldModule,
    MatToolbarModule,
    MatIconModule,
    MatTooltipModule,
    MatSidenavModule,
    MatCardModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    MatDividerModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatExpansionModule,
    MatSelectModule,
    MatDatepickerModule,
    BrowserAnimationsModule,
    FlexLayoutModule,
    SharedModule,
    SettingsModule,
    AppRoutingModule,
    MatGridListModule,
    NgxMatMomentModule,
    NgxMatDatetimePickerModule, NgxMatTimepickerModule
  ],
  providers: [ExchangeService, TranslatePipe],
  bootstrap: [AppComponent]
})
export class AppModule {}
