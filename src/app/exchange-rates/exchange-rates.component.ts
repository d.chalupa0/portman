import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Currency, ExchangeRate } from '../../db/exchange';
import { ExchangeService } from '../services/exchange.service';
import { settingsService } from '../services/settings.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { combineLatest, forkJoin } from 'rxjs';

@Component({
  selector: 'app-exchange-rates',
  templateUrl: './exchange-rates.component.html',
  styleUrls: ['./exchange-rates.component.scss'],
})
export class ExchangeRatesComponent implements OnInit {

  sourceCurrency: Currency;
  allCurrencies: Currency[] = [];
  availableCurrencies: Currency[] = [];

  newCurrencyVisible: boolean = false;
  currencyForm: FormGroup;
  apiPending: boolean = false;
  staleDays: number;

  currencyInputElement!: ElementRef<HTMLInputElement>;

  @ViewChild('newCurrencyName') set inputElRef(elRef: ElementRef<HTMLInputElement>) {
    if (elRef) {
      this.currencyInputElement = elRef;
      setTimeout(() => this.currencyInputElement.nativeElement.focus(), 0);
    }
  };

  constructor(private fb: FormBuilder, private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    combineLatest([
      settingsService.observeMainCurrency(),
      settingsService.observeStaleDayCount(),
      this.exchangeService.observeAllCurrencies()])
    .subscribe(([main, stale, all]) => (
      this.sourceCurrency = main,
      this.staleDays = stale,
      this.allCurrencies = all,
      this.refilterCurrencies()
    ));

    this.currencyForm = this.fb.group({
      newCurrencyName: [null, [Validators.required]]
    });
  }

  refilterCurrencies() {
    this.availableCurrencies = this.allCurrencies.filter((c3) => c3.id !== this.sourceCurrency?.id)
  }

  onAddNewCurrencyClicked() {
    this.newCurrencyVisible = true;
  }

  onFormExit() {
    this.currencyForm.reset();
    this.newCurrencyVisible = false;
  }

  onFormKeyUp(e) {
    if (e.keyCode === 27) {
      this.onFormExit();
    }
  }

  isNameValid(noErrWhenUntouched: boolean = true) {
    return (noErrWhenUntouched && !this.currencyForm.get("newCurrencyName").touched)
           || this.currencyForm.get("newCurrencyName").errors === null;
  }

  onAddNewCurrencySubmit() {
    this.currencyForm.markAllAsTouched();

    if (!this.isNameValid(false))
    {
      return;
    }

    const newCurrency = this.currencyFormToCurrency();
    this.exchangeService.addCurrency(newCurrency);
    this.newCurrencyVisible = false;
  }

  currencyFormToCurrency(): Currency {
    return {
      name: this.currencyForm.value.newCurrencyName
    }
  }

  retrieveClicked() {
    if (!this.sourceCurrency || this.availableCurrencies.length == 0) {
      return;
    }

    this.apiPending = true;

    this.exchangeService.retrieveApiRates(this.sourceCurrency.name, this.availableCurrencies.map((c) => c.name))
    .then((rates) => (
      rates.map(rate => (<ExchangeRate>{
        fromCurrencyId: this.sourceCurrency.id,
        toCurrencyId: this.availableCurrencies.find((v) => v.name == rate.toCurrency).id,
        date: rate.date,
        rate: rate.rate
      }))
    ), () => (
      this.apiPending = false, []))
    .then(ratesWithIds => (
      ratesWithIds.forEach((r) => this.exchangeService.addRate(r),
      this.apiPending = false)
    ));
  }
}
