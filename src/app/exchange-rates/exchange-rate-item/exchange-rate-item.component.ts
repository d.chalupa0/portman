import { Component, Input, OnInit } from '@angular/core';
import * as moment from 'moment';
import { Currency, ExchangeRate } from '../../../db/exchange';
import { ExchangeService } from '../../services/exchange.service';
import { formatDateTimeToLocal, getDifferenceInDays } from '../../utils/date-utils';

@Component({
  selector: 'app-exchange-rate-item',
  templateUrl: './exchange-rate-item.component.html',
  styleUrls: ['./exchange-rate-item.component.scss']
})

export class ExchangeRateItemComponent implements OnInit {

  @Input() sourceCurrency: Currency
  @Input() exchangeCurrency: Currency
  @Input() staleDays: Number

  latestRate: ExchangeRate;
  formattedDate: string;
  isOutdated: boolean = true;
  constructor(private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    if (!this.sourceCurrency) {
      return;
    }

    this.exchangeService.observeLatestRate(this.sourceCurrency.id, this.exchangeCurrency.id)
    .subscribe((r) => (
      this.latestRate = r,
      this.updateDate(),
      this.updateOutdated()));
  }

  updateDate(): void {
    if (this.latestRate)
      this.formattedDate = formatDateTimeToLocal(this.latestRate.date);
  }

  updateOutdated() {
    this.isOutdated = getDifferenceInDays(moment.now(), this.latestRate.date.valueOf()) > this.staleDays;
  }

  onChangedValue(v) {
    this.exchangeService.addRate(
      {
        fromCurrencyId: this.sourceCurrency.id,
        toCurrencyId: this.exchangeCurrency.id,
        date: moment.now(),
        rate: +v
      }
    );
  }
}
