import { Component, OnInit } from '@angular/core';
import { Transaction, Position } from '../../../db';
import { transactionService } from '../../services/transaction.service';

import { positionService } from '../../services/position.service';

interface TransactionWithPositions
{
  transaction: Transaction,
  from: Position,
  to: Position
}

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})

export class TransactionListComponent implements OnInit {

  transactions: TransactionWithPositions[];

  constructor() { }

  ngOnInit(): void {
    transactionService.getTransactions().subscribe((transactions) => (
      this.transactions = [],
      transactions.map(t => 
        Promise.all([this.getPosition(t.fromId), this.getPosition(t.toId)])
        .then((ft) =>
          this.transactions.push({
            transaction: t,
            from: ft[0],
            to: ft[1]
          })
        )
      )
    ))
  }

  getPosition(positionId: number) {
    return positionService.getPosition(positionId);
  }
}
