import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import * as moment from 'moment';
import { Position, Transaction } from '../../../db';
import { positionService } from '../../services/position.service';
import { transactionService } from '../../services/transaction.service';

@Component({
  selector: 'app-transaction-form',
  templateUrl: './transaction-form.component.html',
  styleUrls: ['./transaction-form.component.scss']
})
export class TransactionFormComponent implements OnInit {

  transaction: Transaction = {
    date: moment.now(),
    fromQty: undefined,
    toQty: undefined,
    fee: 0,
    feeCurrencyId: 1
  }

  allPositions: Position[];
  transactionForm: FormGroup;
  goto: string = "/transactions";

  constructor(private router: Router, private fb: FormBuilder, private route: ActivatedRoute) { }

  ngOnInit(): void {
    if (this.route.snapshot.paramMap.has('fromId') || this.route.snapshot.paramMap.has('toId')) {
      this.goto = "/positions";
    }
    this.transaction.fromId = +this.route.snapshot.paramMap.get('fromId');
    this.transaction.toId = +this.route.snapshot.paramMap.get('toId');

    if (this.route.snapshot.paramMap.has('fromQty')) {
      this.transaction.fromQty = +this.route.snapshot.paramMap.get('fromQty');
    }
    this.transactionForm = this.fb.group({
      fromId: [this.transaction.fromId || 0],
      toId: [this.transaction.toId || 0],
      fromQty: [this.transaction.fromQty, [Validators.required]],
      toQty: [this.transaction.toQty, [Validators.required]],
      date: [moment.utc(this.transaction.date).local(), [Validators.required]]
    });

    positionService.getActivePositions().subscribe((positions) => this.allPositions = positions);
  }

  onSubmit()
  {
    this.transaction = this.transactionFormToTransaction();
    transactionService.addTransaction(this.transaction);
    this.router.navigate([this.goto]);
  }

  transactionFormToTransaction(): Transaction {
    return {
      fromId: +this.transactionForm.value.fromId,
      fromQty: +this.transactionForm.value.fromQty,
      toId: +this.transactionForm.value.toId,
      toQty: +this.transactionForm.value.toQty,
      date: moment(this.transactionForm.value.date).valueOf(),
      fee: 0,
      feeCurrencyId: 1
    }
  }

  get fromQty() { return this.transactionForm.get('fromQty'); }
  get toQty() { return this.transactionForm.get('toQty'); }
  get date() { return this.transactionForm.get('date'); }
}
