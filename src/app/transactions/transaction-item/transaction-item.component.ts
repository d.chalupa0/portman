import { Component, Input, OnInit } from '@angular/core';
import { from, Observable, Subscription } from 'rxjs';
import { Transaction, Position } from '../../../db';
import { formatDateTimeToLocal } from '../../utils/date-utils';

import { Currency, ExchangeRate } from '../../../db/exchange';
import { ExchangeService } from '../../services/exchange.service';
import { settingsService } from '../../services/settings.service';
@Component({
  selector: '[app-transaction-item]',
  templateUrl: './transaction-item.component.html',
  styleUrls: ['./transaction-item.component.scss']
})
export class TransactionItemComponent implements OnInit {

  @Input() transaction: Transaction
  @Input() fromPosition: Position
  @Input() toPosition: Position

  fromName: string;
  toName: string;
  formattedDate: string;

  feeExchangeRate: ExchangeRate;
  mainCurrency: Currency;

  changeExchangeRate: boolean = false;

  constructor(private exchangeService: ExchangeService) { }

  ngOnInit(): void {
    this.formattedDate = formatDateTimeToLocal(this.transaction.date);
    this.changeExchangeRate = this.transaction.fromQty < this.transaction.toQty;

    settingsService.observeMainCurrency().subscribe((c) =>
        this.resolveCurrencies(c)
    );
  }

  resolveCurrencies(currency: Currency)
  {
    this.mainCurrency = currency;
    this.fromName = this.fromPosition ? this.fromPosition.name : this.mainCurrency.name;
    this.toName = this.toPosition ? this.toPosition.name : this.mainCurrency.name;

    this.exchangeService.getClosestRate(this.transaction.feeCurrencyId, this.mainCurrency.id, this.transaction
      .date)
    .then((rate) => (this.feeExchangeRate = rate));
  }

  onExchange() {
    this.changeExchangeRate = !this.changeExchangeRate;
  }
}
