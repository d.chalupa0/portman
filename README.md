# Introduction

PortMan - An offline-first portfolio manager

Currently runs with:

- Angular v13.0.0
- Electron v16.0.0


Based on
- https://github.com/maximegris/angular-electron/

## Getting Started

*Clone this repository locally:*

``` bash
git clone https://gitlab.com/d.chalupa0/portman.git
```

*Install dependencies with npm (used by Electron renderer process):*

``` bash
npm install
npm install -g @angular/cli
```

*Install NodeJS dependencies with npm (used by Electron main process):*

``` bash
cd app/
npm install
```

## Run in development

```bash
    npm run ng:serve
```

## Release

- For a web server (result in /dist)
```bash
    ng b --configuration production
```

- As an app (result in /release)
```bash
    npm run electron:build
```

## Project structure

|Folder|Description|
| ---- | ---- |
| app | Electron main process folder (NodeJS) |
| src | Electron renderer process folder (Web / Angular) |